package com.example.design;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    DataAdapter mAdapter;
    RecyclerView mRecyclerView;
    List<Phone> phones = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView)findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        setInitialData();
        // создаем адаптер
        mAdapter = new DataAdapter(this, phones);
        // устанавливаем для списка адаптер
        mRecyclerView.setAdapter(mAdapter);
    }


    private void setInitialData(){

        phones.add(new Phone ("Разработчик 2d", "Я тип чувак жесткий и...", "15m"));
        phones.add(new Phone ("Разработчик 3d", "Прогер тупо че кого", "1d"));
        phones.add(new Phone ("Разработчик 4d", "Тупо рядовой Жмышенко отдельно мех...", "12h"));
        phones.add(new Phone ("Разработчик Nd и вообще ", "Камон я Лосев", "10s"));
    }
}