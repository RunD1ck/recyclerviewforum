package com.example.design;


import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private List<Phone> phones;
    private OnItemClickListener clickListener;

    DataAdapter(Context context, List<Phone> phones) {
        this.phones = phones;
        this.inflater = LayoutInflater.from(context);
    }
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position) {
        Phone phone = phones.get(position);
        holder.whoisView.setText(phone.getWhoIs());
        holder.aboutView.setText(phone.getAbout());
        holder.timeView.setText(phone.getTime());

        
    }

    @Override
    public int getItemCount() {
        return phones.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        final TextView whoisView, aboutView, timeView;
        private int originalHeight = 0;
        private boolean isViewExpanded = false;
        ViewHolder(View view){
            super(view);
            view.setOnClickListener(this);
            whoisView = (TextView) view.findViewById(R.id.whois);
            aboutView = (TextView) view.findViewById(R.id.about);
            timeView = (TextView)view.findViewById(R.id.time);



        }
        @Override
        public void onClick(View v){
            View someview = v.findViewById(R.id.little_view);
            if(someview.getVisibility()==View.GONE){
                someview.setVisibility(View.VISIBLE);
            }else if(someview.getVisibility()==View.VISIBLE){
                someview.setVisibility(View.GONE);
            }
        }


    }
}